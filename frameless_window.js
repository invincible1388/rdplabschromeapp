
var backgroundBlinkInterval;
var launchMode = '';
var testData = {};
var testObject = {};
//launchMode = 'CANDIDATE';
var inviteId = "";

function _toggleClass(element, className){
    if (!element || !className){
        return;
    }

    var classString = element.className, nameIndex = classString.indexOf(className);
    if (nameIndex == -1) {
        classString += ' ' + className;
    }
    else {
        classString = classString.substr(0, nameIndex) + classString.substr(nameIndex+className.length);
    }
    element.className = classString;
}

function _generateGuid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}
 //_fetchTestQuestion(107);
function _init() {
  //_triggerPluginDisplay();
 
}

function _triggerPluginDisplay(testData) {
  if (launchMode === 'CANDIDATE') {
    _setupCandidateView(testData);
  } else {
    _setupRecruiterView(testData);
  }
  document.getElementById("inner-content").style.display = 'block';
}

function _setupRecruiterView(data) {
  var _candidateView = document.querySelector(".candidate-view");
  _candidateView.parentNode.removeChild(_candidateView);
}

function _updateRecruiterView(data) {
  document.querySelector(".recruiter-view .name")
    .textContent = data.candidateName;
  var _score = 0;
  
  //testName
  
  document.querySelector(".recruiter-view .testName")
    .textContent = data.testName;
  var _score = 0;
  if(data.result > 0)
    _score = data.result;

  document.querySelector(".recruiter-view .score")
    .textContent = _score;
  chrome.app.window.current().innerBounds.height=150;
}

function _setupCandidateView(testData) {
	
  _launchCountDownTimer(testData.testDuration);
  var _recruiterView = document.querySelector(".recruiter-view");
  _recruiterView.parentNode.removeChild(_recruiterView);
  //dj 
  //initializeTestObject(107);
  initializeTestObject(testData.testId);
  
  document.getElementById("view-question").addEventListener("click", function(){
	  var testId = '';
	  if(testData.testId)
		  testId = testData.testId;
	  
	  _fetchTestQuestion(testId);

    
  });
  //dont popup the questionbox for now
  // _fetchTestQuestion(testData.testId);
  
  
 

  document.getElementById("finish-test").addEventListener("click", function(){
      //document.getElementById("demo").innerHTML = "Hello World";
	  console.log("finish test");
	//  var c = confirm('Are you done with all your code changes? If so, press OK to mark this test as Finished.');
	 // if(c==true) {
		 _finishTest(); 
	 // }
  });
  chrome.app.window.current().innerBounds.height=250;
  chrome.app.window.current().outerBounds.setPosition(1600,800);
}

function _fetchTestQuestion(_testId) {
  var SERVER_URL='http://www.skillstack.com:3000/api/onlinetests/' + _testId;
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var _testObj = JSON.parse(this.responseText);
	  console.log(_testObj);
	  testObject = _testObj;
      _openQuestionPopup(_testObj);
    }
  };
  xhttp.open("GET", SERVER_URL, true);
  xhttp.setRequestHeader('Content-Type', 'application/json');
  xhttp.send();
}

function _finishTestChromeApp() {
	
	var SERVER_URL='http://www.skillstack.com:3000/api/candidates/createami';
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var _testObj = JSON.parse(this.responseText);
     // _openQuestionPopup(_testObj.description);
	  triggerFinishTest();
	  showNotification('Skillstack', 'Your solution has successfully been submitted.');
    }
  };
  xhttp.open("POST", SERVER_URL, true);
  xhttp.setRequestHeader('Content-Type', 'application/json');
  xhttp.send(JSON.stringify(solution));
}

function _finishTest() {
	//send event to electron app
	//first send beforelogoff event
	sendBeforeLogOffEvent();
	executeTestCases("123");
	if(testObject.enableScreenRecording==true){
		sendFinishTestEvent();
		setTimeout(function(){
			triggerFinishTest();
			showNotification('Skillstack', 'Your solution has successfully been submitted.');		
			},0);
	}
	else{
		console.log("screen recorder settings disabled finish in Chrome app itself")
	  _finishTestChromeApp();
	}
	/*
	var SERVER_URL='http://www.skillstack.com:3000/api/candidates/createami';
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var _testObj = JSON.parse(this.responseText);
     // _openQuestionPopup(_testObj.description);
	  triggerFinishTest();
	  showNotification('skillstack', 'Your solution has successfully been submitted.');
    }
  };
  xhttp.open("POST", SERVER_URL, true);
  xhttp.setRequestHeader('Content-Type', 'application/json');
  xhttp.send(JSON.stringify(solution));
  */
}

function toggle(){
	chrome.runtime.sendMessage({action: 'updateWindow'}, function(updateWindow) {
    console.log(updateWindow);
  });
}

function _openQuestionPopup(_questionText) {
	var updateInfo = {
    width: 300,
    height: 50
};

	console.log("_openQuestionPopup");

  var opts = {
    frame: "none",
    id: "popupQuestion",
    innerBounds: {
        width: 560,
        height: 500,
        minWidth: 220,
        minHeight: 220,
    }
  };
  chrome.app.window.create('printWindow.html', opts,
    function (createdWindow) {
      var win = createdWindow.contentWindow;
      win.onload = function () {
        win.document.querySelector('#description').innerHTML = _questionText.description;
		win.document.querySelector('#name').innerHTML = _questionText.name;
		win.document.querySelector('#hints').innerHTML = _questionText.hints;
		win.document.querySelector('#othernotes').innerHTML = _questionText.otherNotes;
		win.document.getElementById("close-question").addEventListener("click", function(){
	        win.close();
           });
		win.document.getElementById("closeWindow").addEventListener("click", function(){
	        win.close();
           });   
		   
		
        //win.print();
      }
    }
  );
}

function _launchCountDownTimer(endTimeSecs) {
  var deadline = new Date(Date.parse(new Date()) + endTimeSecs * 60 * 1000);
  initializeClock('clockdiv', deadline);
}


function showNotification(title, msg) {
  if(title && msg) {
    var opt = {
      type: "basic",
      title: title,
      message: msg,
      iconUrl: "assets/favicon-32x32.png"
    }
    var id = _generateGuid();
    chrome.notifications.create(id, opt);
    setBlinkingTitle(true);
  } 

}

function setBlinkingTitle(mode) {
  if(mode) {
    var titlebar = document.getElementById("bottom-titlebar");
    backgroundBlinkInterval = setInterval(function(){
      _toggleClass(titlebar, "blink");
    },100);
  } else {
    clearInterval(backgroundBlinkInterval);
    backgroundBlinkInterval = 0;
  }
  
}

var alertPromise
function _alert(messageTitle,messageBody, cb){ 
  console.log("myCustomAlert has been called");
  alertPromise = $.Deferred();

  document.getElementById("alertTitle").textContent=messageTitle;
  document.getElementById("alertText").textContent=messageBody;
  document.getElementById('backgroudDiv').style.display='block';
  
  document.getElementById("alertDone").onclick = function () {
	  console.log("Finally, User clicked Done, \n now we can get back to work..");
	  document.getElementById('backgroudDiv').style.display='none';
	  cb();
		alertPromise.resolve();
		return(false);
	};

  return $.when(alertPromise).done();
}

function setWindowTitle (title) {
  var titlebar = document.querySelector("#bottom-titlebar .bottom-titlebar-text .title").textContent = title;
}

function updateStatusBar(status) {
  var titlebar = document.querySelector("#bottom-titlebar .bottom-titlebar-text .status").textContent = status;
  if(status === 'online') {
    document.getElementById("bottom-titlebaricon")
      .src="green-ico.png";
  } else {
    document.getElementById("bottom-titlebaricon")
      .src="red-ico.png";
  }
}

function focusTitlebars(focus) {
  var bg_color = focus ? "#3a3d3d" : "#7a7c7c";
    
  var titlebar = document.getElementById("bottom-titlebar");
  if (titlebar)
    titlebar.style.backgroundColor = bg_color;
}

window.onfocus = function() { 
  focusTitlebars(true);
  setBlinkingTitle(false);
  console.log("focus");
}

window.onblur = function() {
  //focusTitlebars(false);
  console.log("blur");
}

window.onresize = function() {
  updateContentStyle();
}

window.onload = function() {  

  //connect();
  _init();
  //_launchCountDownTimer(100);
  /*
    document.getElementById("view-question").addEventListener("click", function(){
	  var testId = '';
	  if(testData.testId)
		  testId = testData.testId;
	  
	  _fetchTestQuestion(testId);
   
  });
  */
  
  
   document.getElementById("toggle").addEventListener("click", function(){
	  toggle();  
  });

  document.getElementById("bottom-titlebar-close-button").onclick = function() {
    //window.close();
    console.log("close called..");
    chrome.app.window.current().minimize();
  }
  
}

function _getMachineMode(publicIp) {
	console.log("in get machine details"+publicIp);
  var SERVER_URL='http://www.skillstack.com:3000/api/candidates/gethostmachineuser?id=' + publicIp;
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      //console.log("GOT machine details"+this.responseText);
	  var _machineDetails = JSON.parse(this.responseText);
	  var type = _machineDetails.type;
	  if(type == 'candidate'){
	  launchMode = 'CANDIDATE';
	  inviteId = _machineDetails.inviteId ;
	  console.log("inviteid = "+inviteId);
	  }
	   else
		  launchMode = 'RECRUITER';
	  //now update ui recruiter/candidate-view
	 // _triggerPluginDisplay();
    }
  };
  xhttp.open("GET", SERVER_URL, true);
  xhttp.setRequestHeader('Content-Type', 'application/json');
  xhttp.send();
}

function initializeTestObject(testId){

  var SERVER_URL='http://www.skillstack.com:3000/api/onlinetests/' + testId;
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var _testObj = JSON.parse(this.responseText);
	  console.log("testObject initialized=");
	  console.log(_testObj);
	  testObject = _testObj;
      
    }
  };
  xhttp.open("GET", SERVER_URL, true);
  xhttp.setRequestHeader('Content-Type', 'application/json');
  xhttp.send();

}
