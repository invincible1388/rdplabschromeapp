var chromeWindow;
var maximized=false;

console.log(chrome.windows, chrome.tabs);
chrome.app.runtime.onLaunched.addListener(function() {

  chromeWindow = chrome.app.window.create("frameless_window.html",
    { frame: "none",
      id: "krazykoderWin",
      resizable: false,
	    alwaysOnTop: true,
		state:"minimized",
      innerBounds: {
          width: 300,
          height: 300,
          minWidth: 200,
          minHeight: 70,
          left: screen.availWidth-300,
          top: screen.availHeight-200,
      }
    },
    function(win) {
		console.log(chromeWindow=win);
      win.outerBounds.setPosition(
        screen.availWidth-350, // left
        screen.availHeight - 250 // top
      );
    }
  );
  
});
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
	console.log("on message");
	
  if (request && request.action === 'updateWindow') {
	  console.log("updateWindow");
	   if(maximized){
	 	chromeWindow.outerBounds.setPosition(chromeWindow.getBounds().left,screen.availHeight-70);
	    chromeWindow.outerBounds.setSize(300,70);
		 chromeWindow.setAlwaysOnTop(true);

		maximized = false;
	   }
	   else{
	    chromeWindow.outerBounds.setPosition(chromeWindow.getBounds().left,screen.availHeight-250);
	    chromeWindow.outerBounds.setSize(300,250);
		chromeWindow.setAlwaysOnTop(true);
		maximized = true;
      }
    
  }
});
